const Employee = require('../models/employee');

const EmployeeCtrl = {};

EmployeeCtrl.getEmployees = async (req, res) => {
    const employees = await Employee.find();
    res.json(employees);
    
}

EmployeeCtrl.createEmployee = async (req, res) => {
    const employee = new Employee({
        name: req.body.name,
        position: req.body.position,
        office: req.body.office,
        salary: req.body.salary
    });
    await employee.save();
    res.json({
        'status': 'Empleado guardado'
    });
}

EmployeeCtrl.getEmployee = async (req, res) => {
    const employee = await Employee.findById(req.params.id);
    res.json(employee);
}

EmployeeCtrl.editEmployee = async (req, res) => {
    const employee = {
        name: req.body.name,
        position: req.body.position,
        office: req.body.office,
        salary: req.body.salary
    };
    await Employee.findByIdAndUpdate(req.params.id, {$set: employee}, {new: true});
    res.json({status: 'Employee update'});
}

EmployeeCtrl.deleteEmployee = async (req, res) => {
    await Employee.findByIdAndRemove(req.params.id);
    res.json({status: 'Employee deleted'});
}

module.exports = EmployeeCtrl;