const express = require('express'); 
const morgan = require('morgan');
const cors = require('cors');
const authRoutes = require('./routes/auth.routes');
const properties = require('../config/properties');
const DB = require('../config/database');

const { mongoose } = require('../config/database');
DB();

const app = express();
const router = express.Router();

const bodyParser = require('body-parser');
const bodyParserJSON = bodyParser.json();
const bodyParserURLEnconde = bodyParser.urlencoded({extended: true});

//Middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(cors({origin: 'http://localhost:4200'}));
app.use(bodyParserJSON);
app.use(bodyParserURLEnconde);

//Routes
app.use('/api/employees' ,require('./routes/employee.routes'));
app.use('/api', router);
authRoutes(router);

//Mensaje por defecto
router.get('/', (req, res) => {
    res.send('Hello from home');
});

app.use(router);

// Starting the server
app.listen (properties.PORT, () => console.log(`Server runing on port ${properties.PORT}`));

