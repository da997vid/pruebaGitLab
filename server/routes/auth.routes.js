const Users = require('../controlers/auth.controller');
module.exports = (router) => {
    router.post('/register', Users.createUser);
    router.post('/login', Users.loginUser);
    router.get('/register', Users.getUsers);
}